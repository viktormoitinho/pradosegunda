


        <!DOCTYPE html>
<html>
  <head>
      <title>Transguaru</title>
      <meta charset="utf-8" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Asset&display=swap" rel="stylesheet"> 
      <link rel="stylesheet" href="css/estilo.css">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
     <script type="text/javascript" src="js/popper.min.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/mdb.min.js"></script>
    </head>

  <body>
    <header>
      <?php
        include "navbar.php";
      ?>
    </header>   
     
    <div class="cadastro">

    <?php
    session_start();
    ?>
        <h1>Entre em contato</h1>
        <!-- <a href="cad_usuario.php">Cadastrar</a><br>
        <a href="listar.php">Listar</a><br> -->
        <?php 
        if(isset($_SESSION['msg']))
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        ?>
        <form method="POST" action="index.php">
            <label>Nome: </label><br>
            <input type="text" name="nome" size = "38" maxlength="30" ><br><br>

            <label>E-mail: </label><br>
            <input type="email" name="email" size = "38" maxlength="30" ><br><br>    

            <label>Telefone: </label><br>
            <input type="text" name="telefone" size = "38" maxlength="13"><br><br> 
        
            <label>Orçamento: </label><br>
            <textarea rows="6" cols="40" name="orcamento" >
            </textarea><br><br>

            <input type="submit" value="Cadastrar">
        </form>
</div>
     
      <?php
         include "footer.php";
      ?>

  </body>
</html>

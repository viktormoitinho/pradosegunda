<!DOCTYPE html>
<html>
  <head>
      <title>Transguaru</title>
      <meta charset="utf-8" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Asset&display=swap" rel="stylesheet"> 
      <link rel="stylesheet" href="css/estilo.css">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
     <script type="text/javascript" src="js/popper.min.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/mdb.min.js"></script>
    </head>

  <body>
    <header>
      <?php
        include "navbar.php";
      ?>
    </header>   
      <?php
        include "slider.php";
        include "cards.php";
      ?>
     
      <?php
         include "footer.php";
      ?>

  </body>
</html>

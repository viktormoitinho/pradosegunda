<?php
session_start();
include_once("conexao.php");
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$result_pedidos = "SELECT * FROM pedidos WHERE id = '$id'";
$resultado_usuario = mysqli_query($conn, $result_pedidos);
$row_pedidos = mysqli_fetch_assoc($resultado_usuario);
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta chartset="utf-8">
        <title>CRUD - Editar</title>    
    </head>
    <body>
        <h1>Editar Usuário</h1>
        <?php 
        if(isset($_SESSION['msg']))
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        ?>
        <form method="POST" action="proc_edit_usuario.php">
            <input type="hidden" name="id" value="<?php echo $row_pedidos['id'];?>">   
            
            <label>Nome: </label><br>
            <input type="text" name="nome" placeholder="Digite seu nome completo" value="<?php echo $row_pedidos['nome'];?>"><br><br>

            <label>E-mail: </label><br>
            <input type="email" name="email" placeholder="Digite seu e-mail válido para entrarmos em contato" value="<?php echo $row_pedidos['email'];?>"><br><br>    

            <label>Telefone: </label><br>
            <input type="text" name="telefone" placeholder="Digite seu numero de telefone válido para entrarmos em contato" value="<?php echo $row_pedidos['telefone'];?>"><br><br> 
        
            <label>Orçamento: </label><br>
            <input type="text" name="orcamento" size = "100" placeholder="Digite aqui todos os detalhes sobre o seu pedido de orçamento" value="<?php echo $row_pedidos['orcamento'];?>"><br><br>
            

            <input type="submit" value="Salvar">
        </form>

    </body>
</html>